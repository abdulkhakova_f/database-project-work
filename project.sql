 Publishers
CREATE TABLE Publishers (
    publisher_id INT PRIMARY KEY,
    publisher_name VARCHAR(255) NOT NULL
);

 Prices
CREATE TABLE Prices (
    price_id INT PRIMARY KEY,
    price DECIMAL(10,2) NOT NULL,
    currency VARCHAR(3) NOT NULL
);

  Genres
CREATE TABLE Genres (
    genre_id INT PRIMARY KEY,
    genre_name VARCHAR(255) NOT NULL
);

  Languages
CREATE TABLE Languages (
    language_id INT PRIMARY KEY,
    language_name VARCHAR(255) NOT NULL
);

-Page_Counts
CREATE TABLE Page_Counts (
    page_count_id INT PRIMARY KEY,
    page_count INT NOT NULL
);

  Published_Dates
CREATE TABLE Published_Dates (
    published_date_id INT PRIMARY KEY,
    published_date DATE NOT NULL
);

 Authors
CREATE TABLE Authors (
    author_id INT PRIMARY KEY,
    author_name VARCHAR(255) NOT NULL,
    nationality VARCHAR(255) NOT NULL
);

 Books
CREATE TABLE Books (
    ISBN VARCHAR(255) PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    author VARCHAR(255) NOT NULL,
    rating DECIMAL(3,2) NOT NULL,
    voters INT NOT NULL,
    description TEXT NOT NULL,
    publisher_id INT,
    price_id INT,
    page_count_id INT,
    published_date_id INT,
    FOREIGN KEY (publisher_id) REFERENCES Publishers(publisher_id),
    FOREIGN KEY (price_id) REFERENCES Prices(price_id),
    FOREIGN KEY (page_count_id) REFERENCES Page_Counts(page_count_id),
    FOREIGN KEY (published_date_id) REFERENCES Published_Dates(published_date_id)
);

  Books_Genres
CREATE TABLE Books_Genres (
    book_genre_id INT PRIMARY KEY,
    ISBN VARCHAR(255),
    genre_id INT,
    FOREIGN KEY (ISBN) REFERENCES Books(ISBN),
    FOREIGN KEY (genre_id) REFERENCES Genres(genre_id)
);

  Books_Languages
CREATE TABLE Books_Languages (
    book_language_id INT PRIMARY KEY,
    ISBN VARCHAR(255),
    language_id INT,
    FOREIGN KEY (ISBN) REFERENCES Books(ISBN),
    FOREIGN KEY (language_id) REFERENCES Languages(language_id)
);

 Books_Page_Counts
CREATE TABLE Books_Page_Counts (
    book_page_count_id INT PRIMARY KEY,
    ISBN VARCHAR(255),
    page_count_id INT,
    FOREIGN KEY (ISBN) REFERENCES Books(ISBN),
    FOREIGN KEY (page_count_id) REFERENCES Page_Counts(page_count_id)
);

 Books_Published_Dates
CREATE TABLE Books_Published_Dates (
    book_published_date_id INT PRIMARY KEY,
    ISBN VARCHAR(255),
    published_date_id INT,
    FOREIGN KEY (ISBN) REFERENCES Books(ISBN),
    FOREIGN KEY (published_date_id) REFERENCES Published_Dates(published_date_id)
);

 Books_Authors
CREATE TABLE Books_Authors (
    book_author_id INT PRIMARY KEY,
    ISBN VARCHAR(255),
    author_id INT,
    FOREIGN KEY (ISBN) REFERENCES Books(ISBN),
    FOREIGN KEY (author_id) REFERENCES Authors(author_id)
);
  Reviews
  CREATE TABLE Reviews(
  review_id: INT
    ISBN: VARCHAR(255)
    voter_id: INT
    review_text: TEXT  
    review_date: DATETIME
);